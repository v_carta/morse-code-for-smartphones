package com.example.vanessa.vibcomm;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;

public class Receiving extends AppCompatActivity implements SensorEventListener {

    private TextView result, vibRes, morse;
    int stopCounting = 1;

    private SensorManager senSensorManager;
    private Sensor senAccelerometer;

    private long lastUpdate = 0;
    float SHAKE_THRESHOLD_GRAVITY = 0F; //1.02F, 1.018F

    int brojac = 0;
    long kraj = 0;

    long[] poljePauza;
    long poljeVibracija[]=new long[100];

    long pauze[]=new long[100];
    long vibracije[]=new long[100];

    long finalArray[]=new long[230];

    float average_gForce;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;

    int mode = 0;

    int tockica_dg;
    int tockica_gg;
    int crtica_dg;
    int crtica_gg;
    int pauza_dg;
    int pauza_gg;


    boolean startFlag = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receiving);
        getSupportActionBar().setTitle("Vibration");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        senSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        senAccelerometer = senSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        senSensorManager.registerListener(this, senAccelerometer , SensorManager.SENSOR_DELAY_UI); //17 hz

        result = (TextView) findViewById(R.id.result);
        vibRes = (TextView) findViewById(R.id.vibRes);
        morse  = (TextView) findViewById(R.id.morse);

        prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        RadioButton slow = (RadioButton)findViewById(R.id.slow);
        RadioButton medium = (RadioButton)findViewById(R.id.medium);
        RadioButton fast = (RadioButton)findViewById(R.id.fast);

        mode = prefs.getInt("modeVibReceive", 2);
        if(mode == 1) slow.setChecked(true);
        else if (mode == 2) medium.setChecked(true);
        else if (mode == 3) fast.setChecked(true);

    }


    int i = -1;
    int j = -1;

    long start;
    long end;
    long vibracija;
    boolean flag=true;
    long pocetakVib;

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        Sensor mySensor = sensorEvent.sensor;

        if (mySensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            float x = sensorEvent.values[0];
            float y = sensorEvent.values[1];
            float z = sensorEvent.values[2];

            float gx = x/ senSensorManager.GRAVITY_EARTH;
            float gy = y/ senSensorManager.GRAVITY_EARTH;
            float gz = z/ senSensorManager.GRAVITY_EARTH;

            float gForce = (float) Math.sqrt(gx*gx + gy*gy + gz*gz); //oko 1 kada mobitel miruje

            long curTime = System.currentTimeMillis();

            long diffTime = (curTime - lastUpdate);

            lastUpdate = curTime;
            long diff = 0;
            //Log.d("gForce_mirno", String.valueOf(gForce));

            if ( (gForce > SHAKE_THRESHOLD_GRAVITY)  && stopCounting == 0) {
                brojac++;
                vibRes.setText("Detecting: " + gForce);
                diff = System.currentTimeMillis() - kraj;
                //Log.d("gForce", String.valueOf(gForce));
                if(flag==true) {
                    start = System.currentTimeMillis();
                    flag=false;
                }
                if(kraj!=0 && diff > 3000){
                    if(flag==false) {
                        j++;
                        end = System.currentTimeMillis() - start;
                        vibracija = end - diff;
                        poljeVibracija[j] = vibracija;
                        flag = true;
                    }
                }
                if(diff > 1000){
                    if(flag==false) {
                        j++;
                        pocetakVib = System.currentTimeMillis();
                        end = System.currentTimeMillis() - start;
                        vibracija = end - diff;
                        poljeVibracija[j] = vibracija;
                        flag = true;
                    }
                    brojac=0;
                    poljePauza[++i]=diff;
                }
                kraj = System.currentTimeMillis();
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    long zadnji;
    String dekodirano="";

    public void stop(View view) {
        if (startFlag == true) {
            startFlag = false;

            mode = prefs.getInt("modeVibReceive", 2);
            Log.d("MODE", String.valueOf(mode));
            if (mode == 1) {
                tockica_dg = 200;
                tockica_gg = 1150;
                crtica_dg = 1100;
                crtica_gg = 2500;
                pauza_dg = 2100;
                pauza_gg = 3000;
            } else if (mode == 2) {
                tockica_dg = 200;
                tockica_gg = 900;
                crtica_dg = 900;
                crtica_gg = 2500;
                pauza_dg = 1950;
                pauza_gg = 2500;
            } else if (mode == 3) {
                tockica_dg = 100;
                tockica_gg = 700;
                crtica_dg = 750;
                crtica_gg = 1600;
                pauza_dg = 1650;
                pauza_gg = 2200;
            }

            zadnji = kraj - pocetakVib;
            stopCounting = 1;

            int j = -1;
            for (i = 1; i < poljePauza.length; i++) {
                pauze[++j] = poljePauza[i];
            }
            for (i = 0; i < pauze.length; i++) {
                Log.d("pauza", String.valueOf(pauze[i]));
            }

            poljeVibracija[++j] = zadnji;
            int k = -1;
            for (i = 1; i < poljeVibracija.length; i++) {
                if (poljeVibracija[i] != 0) vibracije[++k] = poljeVibracija[i];
            }
            for (i = 0; i < vibracije.length; i++) {
                Log.d("vibracije", String.valueOf(vibracije[i]));
            }

            int l = -1;
            for (i = 0; i < vibracije.length; i++) {
                finalArray[++l] = vibracije[i];
                finalArray[++l] = pauze[i];
            }

            for (i = 0; i < finalArray.length; i++) {
                Log.d("final", String.valueOf(finalArray[i]));
            }

            Log.d("tockica_dg: ", String.valueOf(tockica_dg));
            Log.d("tockica_gg: ", String.valueOf(tockica_gg));
            Log.d("crtica_dg: ", String.valueOf(crtica_dg));
            Log.d("crtica_gg: ", String.valueOf(crtica_gg));
            Log.d("pauza_dg: ", String.valueOf(pauza_dg));
            Log.d("pauza_gg", String.valueOf(pauza_gg));

            Log.d("dekodirano", String.valueOf(dekodirano));

            for (i = 0; i < finalArray.length; i++) {
                if (i % 2 == 0) {
                    if (finalArray[i] > tockica_dg && finalArray[i] < tockica_gg) dekodirano += '.';
                    else if (finalArray[i] > crtica_dg && finalArray[i] < crtica_gg) dekodirano += '-';
                } else {
                    if (finalArray[i] > pauza_dg && finalArray[i] < pauza_gg) {
                        dekodirano += " ";
                    }
                }
            }

            Log.d("dekodirano", String.valueOf(dekodirano));

            morse.setText("Decoded morse elements:\n" + dekodirano);

            String convertedTxt = MorseCode.morseToAlpha(dekodirano);
            result.setText("Decoded text: " + convertedTxt);
        }

    }

    public void start(View view) {
        startFlag = true;
        average_gForce = prefs.getFloat("average_gForce", 5);
        Log.d("average_gForce: ", String.valueOf(average_gForce));
        SHAKE_THRESHOLD_GRAVITY = average_gForce + 0.0035F;
        Log.d("THRESHOLDER: ", String.valueOf(SHAKE_THRESHOLD_GRAVITY));

        brojac = 0;
        kraj = 0;

        poljePauza = new long[100];

        poljeVibracija = new long[100];

        pauze = new long[100];
        vibracije = new long[100];

        finalArray = new long[230];

        dekodirano="";

        i = -1;
        j = -1;
        vibRes.setText(" ");
        result.setText(" ");
        morse.setText(" ");
        stopCounting = 0;
    }

    public void calibration(View view) {
        Intent calibration = new Intent(Receiving.this, VibCalibration.class);
        startActivity(calibration);
    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.slow:
                if (checked) {
                    editor = prefs.edit();
                    editor.putInt("modeVibReceive", 1);
                    editor.commit();
                    break;
                }
            case R.id.medium:
                if (checked) {
                    editor = prefs.edit();
                    editor.putInt("modeVibReceive", 2);
                    editor.commit();
                    break;
                }
            case R.id.fast:
                if (checked) {
                    editor = prefs.edit();
                    editor.putInt("modeVibReceive", 3);
                    editor.commit();
                    break;
                }
        }
    }

    @Override
    public boolean onSupportNavigateUp(){
        Intent main = new Intent(Receiving.this, Vibration.class);
        startActivity(main);
        return true;
    }

}
