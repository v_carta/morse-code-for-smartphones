package com.example.vanessa.vibcomm;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class Vibration extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vibration);
        getSupportActionBar().setTitle("Vibration");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void send(View view) {
        Intent sending = new Intent(Vibration.this, Sending.class);
        startActivity(sending);
    }

    public void receive(View view) {
        Intent receive = new Intent(Vibration.this, Receiving.class);
        startActivity(receive);
    }

    @Override
    public boolean onSupportNavigateUp(){
        Intent main = new Intent(Vibration.this, MainActivity.class);
        startActivity(main);
        return true;
    }

}
