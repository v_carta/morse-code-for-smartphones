
package com.example.vanessa.vibcomm;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;

public class ReceivingL extends AppCompatActivity {

    TextView textLIGHT_available, textLIGHT_reading, result2, morse;
    private SensorManager mySensorManager;
    private Sensor LightSensor;

    int stopCounting = 1;

    long start;
    long end=0;
    long kraj=0;

    int brojac=0;

    long diff[] = new long[100];
    long diffFinal[] = new long[100];

    long light[] = new long[100];
    long lightFinal[] = new long[100];

    long merge[]=new long[230];
    long mergeFinal[]=new long[230];

    int i=-1;
    int j=-1;

    int tockica_dg;
    int tockica_gg;
    int crtica_dg;
    int crtica_gg;
    int pauza_dg;
    int pauza_gg;

    int averageLight;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;

    int mode = 0;

    boolean startFlag = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receiving_l);
        getSupportActionBar().setTitle("Light");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        textLIGHT_available = (TextView) findViewById(R.id.available);
        textLIGHT_reading = (TextView) findViewById(R.id.reading);
        result2 = (TextView) findViewById(R.id.result2);
        morse = (TextView) findViewById(R.id.morse);

        RadioButton slow = (RadioButton)findViewById(R.id.slow);
        RadioButton medium = (RadioButton)findViewById(R.id.medium);
        RadioButton fast = (RadioButton)findViewById(R.id.fast);

        prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        mode = prefs.getInt("modeLightReceive", 2);
        if(mode == 1) slow.setChecked(true);
        else if (mode == 2) medium.setChecked(true);
        else if (mode == 3) fast.setChecked(true);

        mySensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        LightSensor = mySensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        if (LightSensor != null) {
            mySensorManager.registerListener(
                    LightSensorListener,
                    LightSensor,
                    SensorManager.SENSOR_DELAY_FASTEST);
        } else {
            textLIGHT_available.setText("Sensor.TYPE_LIGHT NOT Available");
        }

    }


    private final SensorEventListener LightSensorListener
            = new SensorEventListener() {

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
            // TODO Auto-generated method stub

        }

        boolean flag=true;


        @Override
        public void onSensorChanged(SensorEvent event) {
            if(event.sensor.getType() == Sensor.TYPE_LIGHT){
                int limit = averageLight + 50;
                if ( (event.values[0] > limit)  && stopCounting == 0) {
                    Log.d("limit: ", String.valueOf(limit));

                    if (flag == true) { //flag je na početku true
                        start = System.currentTimeMillis();
                        diff[++j] = start - kraj;
                        flag = false;
                        brojac++;
                    } else {
                        end = System.currentTimeMillis();
                        kraj=end;
                    }
                }else{
                    flag=true;
                    if(end != 0) {
                        light[++i] = end - start;
                        end=0;
                    }
                }

                textLIGHT_reading.setText("LIGHT: " + event.values[0]);
            }
        }
    };

    String dekodirano="";

    public void start(View view) {
        startFlag = true;
        averageLight = prefs.getInt("averageLight", 50);
        Log.d("averageLight: ", String.valueOf(averageLight));

        brojac = 0;
        kraj = 0;

        light = new long[100];
        lightFinal = new long[100];

        diff = new long[100];
        diffFinal = new long[100];

        merge = new long[230];
        mergeFinal = new long[230];

        dekodirano="";

        i = -1;
        j = -1;

        result2.setText(" ");
        morse.setText(" ");
        stopCounting = 0;
    }

    public void stop(View view) {
        if (startFlag == true) {
            startFlag = false;
            stopCounting = 1;

            mode = prefs.getInt("modeLightReceive", 2);
            Log.d("MODE", String.valueOf(mode));
            if (mode == 1) {
                tockica_dg = 600;
                tockica_gg = 1200;
                crtica_dg = 1300;
                crtica_gg = 2200;
                pauza_dg = 2200;
                pauza_gg = 3200;
            } else if (mode == 2) {
                tockica_dg = 300;
                tockica_gg = 900;
                crtica_dg = 1000;
                crtica_gg = 1900;
                pauza_dg = 1900;
                pauza_gg = 2500;
            } else if (mode == 3) {
                tockica_dg = 200;
                tockica_gg = 500;
                crtica_dg = 550;
                crtica_gg = 1600;
                pauza_dg = 1600;
                pauza_gg = 2200;
            }

            Log.d("tockica_dg: ", String.valueOf(tockica_dg));
            Log.d("tockica_gg: ", String.valueOf(tockica_gg));
            Log.d("crtica_dg: ", String.valueOf(crtica_dg));
            Log.d("crtica_gg: ", String.valueOf(crtica_gg));
            Log.d("pauza_dg: ", String.valueOf(pauza_dg));
            Log.d("pauza_gg", String.valueOf(pauza_gg));

            int j = -1;
            for (int i = 0; i < light.length; i++) {
                if (light[i] != 0) {
                    lightFinal[++j] = light[i];
                    Log.d("light", String.valueOf(light[i]));
                }
            }
            int k = -1;
            for (int i = 1; i < diff.length; i++) {
                if (diff[i] != 0) {
                    diffFinal[++k] = diff[i];
                    Log.d("diff", String.valueOf(diff[i]));
                }
            }
            int l = -1;
            for (int i = 0; i < light.length; i++) {
                merge[++l] = light[i];
                merge[++l] = diffFinal[i];
            }

            int m = -1;
            for (int i = 0; i < merge.length; i++) {
                if (merge[i] != 0) {
                    mergeFinal[++m] = merge[i];
                }
            }

            for (int i = 0; i < mergeFinal.length; i++) {
                if (mergeFinal[i] != 0) {
                    Log.d("final", String.valueOf(mergeFinal[i]));
                    if (i % 2 == 0) {
                        if (mergeFinal[i] > tockica_dg && mergeFinal[i] < tockica_gg)
                            dekodirano += '.';
                        else if (mergeFinal[i] > crtica_dg && mergeFinal[i] < crtica_gg)
                            dekodirano += '-';
                    } else {
                        if (mergeFinal[i] > pauza_dg && mergeFinal[i] < pauza_gg) {
                            dekodirano += " ";
                        }
                    }
                }
            }

            Log.d("dekodirano", String.valueOf(dekodirano));
            morse.setText(String.valueOf("Decoded morse elements:\n" + dekodirano));

            String convertedTxt = MorseCode.morseToAlpha(dekodirano);
            result2.setText(String.valueOf("Decoded text: " + convertedTxt));
        }
    }

    public void calibration(View view) {
        Intent calibration = new Intent(ReceivingL.this, LightCalibration.class);
        startActivity(calibration);
    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.slow:
                if (checked) {
                    editor = prefs.edit();
                    editor.putInt("modeLightReceive", 1);
                    editor.commit();
                    break;
                }
            case R.id.medium:
                if (checked) {
                    editor = prefs.edit();
                    editor.putInt("modeLightReceive", 2);
                    editor.commit();
                    break;
                }
            case R.id.fast:
                if (checked) {
                    editor = prefs.edit();
                    editor.putInt("modeLightReceive", 3);
                    editor.commit();
                    break;
                }
        }
    }

    @Override
    public boolean onSupportNavigateUp(){
        Intent main = new Intent(ReceivingL.this, Light.class);
        startActivity(main);
        return true;
    }
}

