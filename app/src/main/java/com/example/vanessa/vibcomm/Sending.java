package com.example.vanessa.vibcomm;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

public class Sending extends AppCompatActivity {

    private TextView txt;
    private TextView result;
    private Button toMorseBtn;
    Vibrator v;
    long[] pattern;

    public static int tockica;
    public static int crtica;

    public static int pauzaZnak;
    public static int pauzaSlovo;

    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    int mode=0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sending);
        getSupportActionBar().setTitle("Vibration");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        txt = (TextView) findViewById(R.id.txt);
        result = (TextView) findViewById(R.id.result);
        toMorseBtn = (Button) findViewById(R.id.toMorseBtn);

        RadioButton slow = (RadioButton)findViewById(R.id.slow);
        RadioButton medium = (RadioButton)findViewById(R.id.medium);
        RadioButton fast = (RadioButton)findViewById(R.id.fast);

        prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        mode = prefs.getInt("modeVibSend", 2);

        if (mode == 1) slow.setChecked(true);
        else if (mode == 2) medium.setChecked(true);
        else if (mode == 3) fast.setChecked(true);

        toMorseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt.onEditorAction(EditorInfo.IME_ACTION_DONE);
                if (!txt.getText().toString().isEmpty()) {
                    String txtToConvert = txt.getText().toString();
                    String convertedTxt = MorseCode.alphaToMorse(txtToConvert);
                    convertedTxt = convertedTxt.substring(0, convertedTxt.length() - 3);
                    result.setText("Morse elements:\n" + convertedTxt);

                    mode = prefs.getInt("modeVibSend", 2);

                    if (mode == 1) {
                        tockica = 1300;
                        crtica = 1900;
                        pauzaZnak = 1600;
                        pauzaSlovo = 2300;
                    } else if (mode == 2) {
                        tockica = 1000;
                        crtica = 1600;
                        pauzaZnak = 1300;
                        pauzaSlovo = 2000;
                    } else if (mode == 3) {
                        tockica = 700;
                        crtica = 1300;
                        pauzaZnak = 1000;
                        pauzaSlovo = 1700;
                    }

                    Log.d("mode: ", String.valueOf(mode));
                    Log.d("tockica: ", String.valueOf(tockica));
                    Log.d("crtica: ", String.valueOf(crtica));

                    pattern = new long[100];
                    pattern[0] = 0;
                    char prethodni = ' ';
                    int j = 1;
                    for (int i = 0; i < convertedTxt.length(); i++) {
                        if (i == 0) {
                            if (convertedTxt.charAt(i) == '-') pattern[j++] = crtica;
                            else pattern[j++] = tockica;
                        } else {
                            if (convertedTxt.charAt(i) == '.' && (prethodni == '-' || prethodni == '.')) {
                                pattern[j++] = pauzaZnak;
                                pattern[j++] = tockica;
                            } else if (convertedTxt.charAt(i) == '-' && (prethodni == '-' || prethodni == '.')) {
                                pattern[j++] = pauzaZnak;
                                pattern[j++] = crtica;
                            } else if (convertedTxt.charAt(i) == ' ' && (prethodni == '-' || prethodni == '.')) {
                                pattern[j++] = pauzaSlovo;
                            } else if (convertedTxt.charAt(i) == '.' && prethodni == ' ') {
                                pattern[j++] = tockica;
                            } else if (convertedTxt.charAt(i) == '-' && prethodni == ' ') {
                                pattern[j++] = crtica;
                            }
                        }
                        prethodni = convertedTxt.charAt(i);
                    }
                    Log.d("element", convertedTxt);

                    for (int i = 0; i < pattern.length; i++) {
                        Log.d("element", Long.toString(pattern[i]));
                    }

                    v.vibrate(pattern, -1);
                }

            }
        });
    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.slow:
                if (checked) {
                    editor = prefs.edit();
                    editor.putInt("modeVibSend", 1);
                    editor.commit();
                    break;
                }
            case R.id.medium:
                if (checked) {
                    editor = prefs.edit();
                    editor.putInt("modeVibSend", 2);
                    editor.commit();
                    break;
                }
            case R.id.fast:
                if (checked) {
                    editor = prefs.edit();
                    editor.putInt("modeVibSend", 3);
                    editor.commit();
                    break;
                }
        }
    }

    @Override
    public boolean onSupportNavigateUp(){
        Intent main = new Intent(Sending.this, Vibration.class);
        startActivity(main);
        return true;
    }
}
