package com.example.vanessa.vibcomm;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;


public class Light extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle("Light");
        setContentView(R.layout.activity_vibration);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    public void send(View view) {
        Intent sendingL = new Intent(Light.this, SendingL.class);
        startActivity(sendingL);
    }

    public void receive(View view) {
        Intent receiveL = new Intent(Light.this, ReceivingL.class);
        startActivity(receiveL);
    }

    @Override
    public boolean onSupportNavigateUp(){
        Intent main = new Intent(Light.this, MainActivity.class);
        startActivity(main);
        return true;
    }

}
