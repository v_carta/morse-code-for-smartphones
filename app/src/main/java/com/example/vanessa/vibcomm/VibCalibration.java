package com.example.vanessa.vibcomm;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class VibCalibration extends AppCompatActivity implements SensorEventListener {

    private TextView countdown, vib_reading;

    private SensorManager senSensorManager;
    private Sensor senAccelerometer;

    int average;
    SharedPreferences prefs;

    CountDownTimer cTimer = null;
    int counter = 0;
    double sum = 0;
    boolean flag = true;
    float result = 0;

    boolean timerFlag = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vib_calibration);
        getSupportActionBar().setTitle("Vibration");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        senSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        senAccelerometer = senSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        senSensorManager.registerListener(this, senAccelerometer , SensorManager.SENSOR_DELAY_UI); //17 hz

        vib_reading = (TextView) findViewById(R.id.reading);
        countdown = (TextView) findViewById(R.id.countdown);

    }



    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        Sensor mySensor = sensorEvent.sensor;

        if (mySensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            float x = sensorEvent.values[0];
            float y = sensorEvent.values[1];
            float z = sensorEvent.values[2];

            float gx = x/ senSensorManager.GRAVITY_EARTH;
            float gy = y/ senSensorManager.GRAVITY_EARTH;
            float gz = z/ senSensorManager.GRAVITY_EARTH;

            float gForce = (float) Math.sqrt(gx*gx + gy*gy + gz*gz); //oko 1 kada mobitel miruje

            vib_reading.setText("Sensor readings: " + gForce);
            if (flag == true){
                sum += gForce;
                counter++;
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public void startTimer(View view) {
        if(timerFlag == false) {
            timerFlag = true;
            cTimer = new CountDownTimer(30000, 1000) {
                public void onTick(long millisUntilFinished) {
                    countdown.setText("Seconds remaining: " + millisUntilFinished / 1000);
                }

                public void onFinish() {
                    flag = false;
                    result = (float) sum / counter;
                    countdown.setText("Average: " + result);

                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putFloat("average_gForce", result);
                    editor.commit();

                    float average_gForce = prefs.getFloat("average_gForce", 5);
                    Log.d("average_gForce", String.valueOf(average_gForce));

                    result = 0;
                    sum = 0;
                    counter = 0;
                    timerFlag = false;
                    flag = true;
                }
            };
            cTimer.start();
        }
    }

    @Override
    public boolean onSupportNavigateUp(){
        Intent main = new Intent(VibCalibration.this, Receiving.class);
        startActivity(main);
        return true;
    }

}
