package com.example.vanessa.vibcomm;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class SendingL extends AppCompatActivity {

    private TextView txt;
    private TextView result;
    private Button toMorseBtn;
    long[] pattern;

    int tockica;
    int crtica;

    int pauzaZnak;
    int pauzaSlovo;

    private boolean flashLightStatus = false;
    private static final int CAMERA_REQUEST = 50;
    String cameraId;

    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    int mode=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sending_l);
        getSupportActionBar().setTitle("Light");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ActivityCompat.requestPermissions(SendingL.this, new String[] {Manifest.permission.CAMERA}, CAMERA_REQUEST);

        txt = (TextView) findViewById(R.id.txt);
        result = (TextView) findViewById(R.id.result);
        toMorseBtn = (Button) findViewById(R.id.toMorseBtn);
        cameraId = null;

        RadioButton slow = (RadioButton)findViewById(R.id.slow);
        RadioButton medium = (RadioButton)findViewById(R.id.medium);
        RadioButton fast = (RadioButton)findViewById(R.id.fast);

        prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        mode = prefs.getInt("modeLightSend", 2);
        if (mode == 1) slow.setChecked(true);
        else if (mode == 2) medium.setChecked(true);
        else if (mode == 3) fast.setChecked(true);

        final boolean hasCameraFlash = getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
        boolean isEnabled = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;


        toMorseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt.onEditorAction(EditorInfo.IME_ACTION_DONE);
                if (!txt.getText().toString().isEmpty()) {
                    String txtToConvert = txt.getText().toString();
                    String convertedTxt = MorseCode.alphaToMorse(txtToConvert);
                    convertedTxt = convertedTxt.substring(0, convertedTxt.length() - 3);
                    result.setText("Morse elements:\n" + convertedTxt);

                    mode = prefs.getInt("modeLightSend", 2);
                    //result.setText(String.valueOf(mode));
                    Log.d("MODE", String.valueOf(mode));


                    if (mode == 1) {
                        tockica = 1100;
                        crtica = 1800;
                        pauzaZnak = 1600;
                        pauzaSlovo = 2300;
                    } else if (mode == 2) {
                        tockica = 800;
                        crtica = 1500;
                        pauzaZnak = 1300;
                        pauzaSlovo = 2000;
                    } else if (mode == 3) {
                        tockica = 500;
                        crtica = 1200;
                        pauzaZnak = 1000;
                        pauzaSlovo = 1700;
                    }

                    Log.d("tockica: ", String.valueOf(tockica));
                    Log.d("crtica: ", String.valueOf(crtica));


                    pattern = new long[100];
                    pattern[0] = 0;
                    char prethodni = ' ';
                    int j = 1;
                    for (int i = 0; i < convertedTxt.length(); i++) {
                        if (i == 0) {
                            if (convertedTxt.charAt(i) == '-') pattern[j++] = crtica;
                            else pattern[j++] = tockica;
                        } else {
                            if (convertedTxt.charAt(i) == '.' && (prethodni == '-' || prethodni == '.')) {
                                pattern[j++] = pauzaZnak;
                                pattern[j++] = tockica;
                            } else if (convertedTxt.charAt(i) == '-' && (prethodni == '-' || prethodni == '.')) {
                                pattern[j++] = pauzaZnak;
                                pattern[j++] = crtica;
                            } else if (convertedTxt.charAt(i) == ' ' && (prethodni == '-' || prethodni == '.')) {
                                pattern[j++] = pauzaSlovo;
                            } else if (convertedTxt.charAt(i) == '.' && prethodni == ' ') {
                                pattern[j++] = tockica;
                            } else if (convertedTxt.charAt(i) == '-' && prethodni == ' ') {
                                pattern[j++] = crtica;
                            }
                        }
                        prethodni = convertedTxt.charAt(i);
                    }
                    Log.d("element", convertedTxt);
                    new LongOperation().execute("");
                }
            }
        });
    }

    private class LongOperation extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            CameraManager cameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);

            try {
                cameraId = cameraManager.getCameraIdList()[0];
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
            for(int i=1; i<pattern.length-1; i++){
                Log.d("element",  Long.toString(pattern[i]));
                try {
                    cameraManager.setTorchMode(cameraId, true);
                } catch (CameraAccessException e) {
                    e.printStackTrace();
                }
                flashLightStatus = true;
                try {
                    Thread.sleep(pattern[i]);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                i++;
                if(i<pattern.length && pattern[i]!=0) {
                    try {
                        cameraManager.setTorchMode(cameraId, false);
                    } catch (CameraAccessException e) {
                        e.printStackTrace();
                    }
                    flashLightStatus = false;
                    try {
                        Thread.sleep(pattern[i]);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }else{
                    try {
                        cameraManager.setTorchMode(cameraId, false);
                    } catch (CameraAccessException e) {
                        e.printStackTrace();
                    }
                    flashLightStatus = false;
                    break;
                }
            }
            return null;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch(requestCode) {
            case CAMERA_REQUEST :
                if (grantResults.length > 0  &&  grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                    Toast.makeText(SendingL.this, "Permission Denied for the Camera", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.slow:
                if (checked) {
                    editor = prefs.edit();
                    editor.putInt("modeLightSend", 1);
                    editor.commit();
                    break;
                }
            case R.id.medium:
                if (checked) {
                    editor = prefs.edit();
                    editor.putInt("modeLightSend", 2);
                    editor.commit();
                    break;
                }
            case R.id.fast:
                if (checked) {
                    editor = prefs.edit();
                    editor.putInt("modeLightSend", 3);
                    editor.commit();
                    break;
                }
        }
    }

    @Override
    public boolean onSupportNavigateUp(){
        Intent main = new Intent(SendingL.this, Light.class);
        startActivity(main);
        return true;
    }
}
