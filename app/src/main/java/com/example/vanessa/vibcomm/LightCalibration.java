package com.example.vanessa.vibcomm;

import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class LightCalibration extends AppCompatActivity {

    TextView textLIGHT_reading, textLIGHT_available, countdown;
    private SensorManager mySensorManager;
    private Sensor LightSensor;
    CountDownTimer cTimer = null;
    int counter = 0;
    double sum = 0;
    boolean flag = true;
    double result = 0;

    boolean timerFlag = false;

    public static final String MY_PREFS_NAME = "MyPrefsFile";

    SharedPreferences prefs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle("Light");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setContentView(R.layout.activity_light_calibration);
        textLIGHT_reading = (TextView) findViewById(R.id.reading);
        textLIGHT_available = (TextView) findViewById(R.id.available);
        countdown = (TextView) findViewById(R.id.countdown);

        mySensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        LightSensor = mySensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        if (LightSensor != null) {
            mySensorManager.registerListener(
                    LightSensorListener,
                    LightSensor,
                    SensorManager.SENSOR_DELAY_FASTEST);
        } else {
            textLIGHT_available.setText("Sensor.TYPE_LIGHT NOT Available");
        }

        prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
    }


    private final SensorEventListener LightSensorListener
            = new SensorEventListener() {

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onSensorChanged(SensorEvent event) {
            if(event.sensor.getType() == Sensor.TYPE_LIGHT){
                textLIGHT_reading.setText("Lux readings: " + event.values[0]);
                if (flag == true){
                    sum += event.values[0];
                    counter++;
                }
            }
        }
    };

    public void startTimer(View view){
        if(timerFlag == false) {
            timerFlag = true;
            cTimer = new CountDownTimer(10000, 1000) {
                public void onTick(long millisUntilFinished) {
                    countdown.setText(" Seconds remaining: " + millisUntilFinished / 1000);
                }

                public void onFinish() {
                    flag = false;
                    result = sum/counter;
                    countdown.setText(" Average: " + Math.round(result));

                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putInt("averageLight", (int) Math.round(result));
                    editor.commit();

                    int averageLight = prefs.getInt("averageLight", 50);
                    Log.d("averageLight", String.valueOf(averageLight));

                    result = 0;
                    sum = 0;
                    counter = 0;
                    timerFlag = false;
                    flag = true;
                }
            };
            cTimer.start();
        }
    }

    @Override
    public boolean onSupportNavigateUp(){
        Intent main = new Intent(LightCalibration.this, ReceivingL.class);
        startActivity(main);
        return true;
    }
}
