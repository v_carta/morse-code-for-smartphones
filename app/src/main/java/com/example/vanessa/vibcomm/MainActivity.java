package com.example.vanessa.vibcomm;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void vibration(View view) {
        Intent vibration = new Intent(MainActivity.this, Vibration.class);
        startActivity(vibration);
    }

    public void light(View view) {
        Intent light = new Intent(MainActivity.this, Light.class);
        startActivity(light);
    }
}
